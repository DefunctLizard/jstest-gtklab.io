all:
	mkdir -p build
	cp -v *.css *.html *.png *.tar.bz2 build

publish:
	./publish.sh

.PHONY: all publish


# EOF #
